/*
 * kl25z_board.h
 *
 *  Created on: 26 de set de 2018
 *      Author: plinio
 */

#ifndef KL25Z_BOARD_H_
#define KL25Z_BOARD_H_

#include "MKL25Z4.h"


#define LED_RED_GPIO		GPIOB
#define LED_RED_PIN			18U

#define LED_GREEN_GPIO		GPIOB
#define LED_GREEN_PIN		19U

#define LED_BLUE_GPIO		GPIOD
#define LED_BLUE_PIN		1U


void Board_Init_Peripherals(void);

#endif /* KL25Z_BOARD_H_ */
