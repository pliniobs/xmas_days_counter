/*
 * kl25z_board.c
 *
 *  Created on: 26 de set de 2018
 *      Author: plinio
 */

#include "kl25z_board.h"
#include "fsl_common.h"
#include "fsl_port.h"
#include "fsl_gpio.h"

gpio_pin_config_t output_config = {kGPIO_DigitalOutput, 1};

void Board_Init_Peripherals(void)
{
	CLOCK_EnableClock(kCLOCK_PortB);                           /* Port B Clock Gate Control: Clock enabled */
	CLOCK_EnableClock(kCLOCK_PortD);                           /* Port D Clock Gate Control: Clock enabled */

	PORT_SetPinMux(PORTB, LED_RED_PIN, 		kPORT_MuxAsGpio);         // PORTB18
	PORT_SetPinMux(PORTB, LED_GREEN_PIN, 	kPORT_MuxAsGpio);         // PORTB19
	PORT_SetPinMux(PORTD, LED_BLUE_PIN, 	kPORT_MuxAsGpio);         // PORTD1

	GPIO_PinInit(LED_RED_GPIO, 		LED_RED_PIN, &output_config); 	// LED RED
	GPIO_PinInit(LED_GREEN_GPIO, 	LED_GREEN_PIN, &output_config); // LED GREEN
	GPIO_PinInit(LED_BLUE_GPIO,  	LED_BLUE_PIN, &output_config); 	// LED BLUE
}
