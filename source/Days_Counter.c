#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "kl25z_board.h"
#include "fsl_pit.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#include "kl25z_board.h"
#include "fsl_common.h"
#include "fsl_port.h"
#include "fsl_gpio.h"

static void Task_LEDs_Blink(void *pvParameters);
static void Task_Sensor_Manager(void *pvParameters);

gpio_pin_config_t sensor_pin_config = {kGPIO_DigitalOutput, 1};
uint32_t Tick_Counter = 0;

int main(void) {

	pit_config_t pitConfig;		/* Structure of initialize PIT */

	/* Init board hardware. */
	BOARD_InitBootPins();
	BOARD_InitBootClocks();
	BOARD_InitBootPeripherals();
	/* Init FSL debug console. */
	BOARD_InitDebugConsole();

	PRINTF("Hello World\n");

	Board_Init_Peripherals();

	PIT_GetDefaultConfig(&pitConfig);
	/* Init pit module */
	PIT_Init(PIT, &pitConfig);
	/* Set timer period for channel 0 */
	PIT_SetTimerPeriod(PIT, kPIT_Chnl_0, USEC_TO_COUNT(50U, CLOCK_GetFreq(kCLOCK_BusClk)));
	/* Enable timer interrupts for channel 0 */
	PIT_EnableInterrupts(PIT, kPIT_Chnl_0, kPIT_TimerInterruptEnable);
	/* Enable at the NVIC */
	EnableIRQ(PIT_IRQn);
	/* Start channel 0 */
	PIT_StartTimer(PIT, kPIT_Chnl_0);






	xTaskCreate(Task_LEDs_Blink, "Blink_Task", configMINIMAL_STACK_SIZE, NULL, configMAX_PRIORITIES - 1, NULL);
	xTaskCreate(Task_Sensor_Manager, "Sensor_Task", configMINIMAL_STACK_SIZE, NULL, configMAX_PRIORITIES - 1, NULL);




	vTaskStartScheduler();
	while(1);
	return 0;
}


void PIT_IRQHandler(void)
{
	/* Clear interrupt flag.*/
	PIT_ClearStatusFlags(PIT, kPIT_Chnl_0, kPIT_TimerFlag);
	Tick_Counter ++;
}

static void Task_Sensor_Manager(void *pvParameters)
{
	enum {INIT_COMM, GEN_START, GET_DATA} Sensor_CTRL_State_Machine = INIT_COMM;
	uint32_t Start_Tick;

	PORT_SetPinMux(PORTD, 4u,  kPORT_MuxAsGpio);         // PORTD4

	GPIO_PinInit(GPIOD, 4U, &sensor_pin_config);
	GPIO_WritePinOutput(GPIOD, 4U, 1U);

	while(1)
	{
		switch (Sensor_CTRL_State_Machine) {
		case INIT_COMM:
			sensor_pin_config.pinDirection = kGPIO_DigitalOutput;
			GPIO_PinInit(GPIOD, 4U, &sensor_pin_config);

			GPIO_WritePinOutput(GPIOD, 4U, 0U);
			Start_Tick = Tick_Counter;
			Sensor_CTRL_State_Machine = GEN_START;
			break;
		case GEN_START:
			if((Tick_Counter - Start_Tick) > 10)
			{
				GPIO_WritePinOutput(GPIOD, 4U, 1U);
				sensor_pin_config.pinDirection = kGPIO_DigitalInput;
				GPIO_PinInit(GPIOD, 4U, &sensor_pin_config);
				Sensor_CTRL_State_Machine = GET_DATA;
			}
			break;
		case GET_DATA:
			vTaskDelay(2000/portTICK_PERIOD_MS);
			Sensor_CTRL_State_Machine = INIT_COMM;
			break;
		}

	}

}
/*!
 * @brief Task used to blink the leds
 */
static void Task_LEDs_Blink(void *pvParameters)
{
	enum {BLINK_RED_LED, BLINK_GREEN_LED, BLINK_BLUE_LED} LED_CTRL_State_Machine = BLINK_RED_LED;

	for (;;)
	{
		switch (LED_CTRL_State_Machine) {
		case BLINK_RED_LED:
			GPIO_WritePinOutput(LED_RED_GPIO, LED_RED_PIN, 0U);
			vTaskDelay(250/portTICK_PERIOD_MS);
			GPIO_WritePinOutput(LED_RED_GPIO, LED_RED_PIN, 1U);
			vTaskDelay(250/portTICK_PERIOD_MS);
			LED_CTRL_State_Machine = BLINK_GREEN_LED;
			break;
		case BLINK_GREEN_LED:
			GPIO_WritePinOutput(LED_GREEN_GPIO, LED_GREEN_PIN, 0U);
			vTaskDelay(250/portTICK_PERIOD_MS);
			GPIO_WritePinOutput(LED_GREEN_GPIO, LED_GREEN_PIN, 1U);
			vTaskDelay(250/portTICK_PERIOD_MS);
			LED_CTRL_State_Machine = BLINK_BLUE_LED;
			break;
		case BLINK_BLUE_LED:
			GPIO_WritePinOutput(LED_BLUE_GPIO, LED_BLUE_PIN, 0U);
			vTaskDelay(250/portTICK_PERIOD_MS);
			GPIO_WritePinOutput(LED_BLUE_GPIO, LED_BLUE_PIN, 1U);
			vTaskDelay(250/portTICK_PERIOD_MS);
			LED_CTRL_State_Machine = BLINK_RED_LED;
			break;
		}
	}
}

